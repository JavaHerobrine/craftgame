package io.github.javaherobrine.net;
public enum TransmissionFormat {
	OBJECT,JSON,FINISH,@Deprecated RECONNECT
}
